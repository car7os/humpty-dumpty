# Problema HumptyDumpty

Escribe una funcion que reciba un numero `n`, que indica el limite superior de una lista de los números del `1 a n`.

La función debe retornar dicha lista con sus elementos en forma de cadena, pero por cada múltiplo de 3 debe ser sustituido por '_Humpty_' en vez del número, y para los múltiplos de 5, debe ser sustituido por '_Dumpty_' en vez del número. Para los números que son múltiplos tanto de 3 como de 5 debe ser sustituido por '_HumptyDumpty_'.

Por ejemplo:

Si la función recibe el número `n=15`, debería retornar:

```
["1",
 "2",
 "Humpty",
 "4",
 "Dumpty",
 "Humpty",
 "7",
 "8",
 "Humpty",
 "Dumpty",
 "11",
 "Humpty",
 "13",
 "14",
 "HumptyDumpty"
]
```

El problema es agnóstico de lenguaje, usa el que se te indique en el issue que se te asigne. O libremente si no se da ninguna indicación

## INSTRUCCIONES:

[Sustituye esta línea con las indicaciones para ejecutar tu programa (lenguaje, ambiente, versiones, comandos, etc.)]
